/*
    Copyright � 2007, The AROS Development Team. All rights reserved.
    $Id$
*/

#include <aros/debug.h>

#include <pthread.h>

int pthread_continue_np(pthread_t thread)
{
#   warning Implement pthread_continue_np()
    AROS_FUNCTION_NOT_IMPLEMENTED("pthread_continue_np");
	
    return 0;
}
