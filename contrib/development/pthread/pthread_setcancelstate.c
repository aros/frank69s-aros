/*
    Copyright � 2007, The AROS Development Team. All rights reserved.
    $Id$
*/

#include <aros/debug.h>

#include <pthread.h>

int pthread_setcancelstate(int newstate, int* oldstate)
{
#   warning Implement pthread_setcancelstate()
    AROS_FUNCTION_NOT_IMPLEMENTED("pthread_setcancelstate");
	
    return 0;
}
