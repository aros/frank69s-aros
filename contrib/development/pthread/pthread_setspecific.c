/*
    Copyright © 2011, The AROS Development Team. All rights reserved.
    $Id$
*/

#include <aros/debug.h>
#include <pthread.h>

int   pthread_setspecific(pthread_key_t k, const void * s)
{
#   warning Implement pthread_setspecific()
    AROS_FUNCTION_NOT_IMPLEMENTED("pthread_setspecific");
    return 0;
}
