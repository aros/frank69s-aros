/*
    Copyright © 2011, The AROS Development Team. All rights reserved.
    $Id$
*/

#include <aros/debug.h>
#include <pthread.h>

int   pthread_cond_broadcast(pthread_cond_t * c)
{
#   warning Implement pthread_cond_broadcast()
    AROS_FUNCTION_NOT_IMPLEMENTED("pthread_cond_broadcast");
    return 0;
}
