/*
    Copyright � 2007, The AROS Development Team. All rights reserved.
    $Id$
*/

#include <aros/debug.h>

#include <pthread.h>

int pthread_attr_setstacksize(pthread_attr_t* attr, size_t stacksize)
{
#   warning Implement pthread_attr_setstacksize()
    AROS_FUNCTION_NOT_IMPLEMENTED("pthread_attr_setstacksize");
	
    return 0;
}
