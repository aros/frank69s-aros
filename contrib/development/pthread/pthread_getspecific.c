/*
    Copyright © 2011, The AROS Development Team. All rights reserved.
    $Id$
*/

#include <aros/debug.h>
#include <pthread.h>

void *pthread_getspecific(pthread_key_t k)
{
#   warning Implement pthread_getspecific()
    AROS_FUNCTION_NOT_IMPLEMENTED("pthread_getspecific");
    return 0;
}
