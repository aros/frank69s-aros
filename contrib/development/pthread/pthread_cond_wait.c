/*
    Copyright © 2011, The AROS Development Team. All rights reserved.
    $Id$
*/

#include <aros/debug.h>
#include <pthread.h>

int   pthread_cond_wait(pthread_cond_t * c, pthread_mutex_t * m)
{
#   warning Implement pthread_cond_wait()
    AROS_FUNCTION_NOT_IMPLEMENTED("pthread_cond_wait");
    return 0;
}
