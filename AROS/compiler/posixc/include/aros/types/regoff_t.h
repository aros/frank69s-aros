#ifndef _AROS_TYPES_REGOFF_T_H
#define _AROS_TYPES_REGOFF_T_H

/*
    Copyright © 2010-2011, The AROS Development Team. All rights reserved.
    $Id: /aros/branches/ABI_V1/trunk-aroscsplit/AROS/compiler/arosnixc/include/aros/types/regoff_t.h 35134 2010-10-23T10:55:40.569523Z verhaegs  $

    off_t type definition
*/

#include <aros/cpu.h>

typedef	signed AROS_32BIT_TYPE regoff_t;

#endif /* _AROS_TYPES_REGOFF_T_H */
