#define VERSION  52
#define REVISION 31
#define DATE     "21.2.2012"
#define VERS     "DiskImageGUI 52.31"
#define VSTRING  "DiskImageGUI 52.31 (21.2.2012)\r\n"
#define VERSTAG  "\0$VER: DiskImageGUI 52.31 (21.2.2012)"
