##begin config
basename Expat
version 2.1
date 06.06.2012
copyright Copyright (C) 2001-2012 Expat maintainers, 2012 The AROS Development Team
options pertaskbase
rellib stdc
##end config

##begin cdef
#include <expat.h>
##end cdef

##begin functionlist
XML_Parser XML_ParserCreate(const XML_Char * encodingName)
XML_Parser XML_ParserCreateNS(const XML_Char * encodingName, XML_Char nsSep)
XML_Parser XML_ParserCreate_MM(const XML_Char * encodingName, const XML_Memory_Handling_Suite * memsuite, const XML_Char * nameSep)
XML_Parser XML_ExternalEntityParserCreate(XML_Parser oldParser, const XML_Char * context, const XML_Char * encodingName)
void XML_ParserFree(XML_Parser parser)
enum XML_Status XML_Parse(XML_Parser parser, const char * s, int len, int isFinal)
enum XML_Status XML_ParseBuffer(XML_Parser parser, int len, int isFinal)
void * XML_GetBuffer(XML_Parser parser, int len)
void XML_SetStartElementHandler(XML_Parser parser, XML_StartElementHandler start)
void XML_SetEndElementHandler(XML_Parser parser, XML_EndElementHandler end)
void XML_SetElementHandler(XML_Parser parser, XML_StartElementHandler start, XML_EndElementHandler end)
void XML_SetCharacterDataHandler(XML_Parser parser, XML_CharacterDataHandler handler)
void XML_SetProcessingInstructionHandler(XML_Parser parser, XML_ProcessingInstructionHandler handler)
void XML_SetCommentHandler(XML_Parser parser, XML_CommentHandler handler)
void XML_SetStartCdataSectionHandler(XML_Parser parser, XML_StartCdataSectionHandler start)
void XML_SetEndCdataSectionHandler(XML_Parser parser, XML_EndCdataSectionHandler end)
void XML_SetCdataSectionHandler(XML_Parser parser, XML_StartCdataSectionHandler start, XML_EndCdataSectionHandler end)
void XML_SetDefaultHandler(XML_Parser parser, XML_DefaultHandler handler)
void XML_SetDefaultHandlerExpand(XML_Parser parser, XML_DefaultHandler handler)
void XML_SetExternalEntityRefHandler(XML_Parser parser, XML_ExternalEntityRefHandler handler)
void XML_SetExternalEntityRefHandlerArg(XML_Parser parser, void * arg)
void XML_SetUnknownEncodingHandler(XML_Parser parser, XML_UnknownEncodingHandler handler, void * data)
void XML_SetStartNamespaceDeclHandler(XML_Parser parser, XML_StartNamespaceDeclHandler start)
void XML_SetEndNamespaceDeclHandler(XML_Parser parser, XML_EndNamespaceDeclHandler end)
void XML_SetNamespaceDeclHandler(XML_Parser parser, XML_StartNamespaceDeclHandler start, XML_EndNamespaceDeclHandler end)
void XML_SetXmlDeclHandler(XML_Parser parser, XML_XmlDeclHandler handler)
void XML_SetStartDoctypeDeclHandler(XML_Parser parser, XML_StartDoctypeDeclHandler start)
void XML_SetEndDoctypeDeclHandler(XML_Parser parser, XML_EndDoctypeDeclHandler end)
void XML_SetDoctypeDeclHandler(XML_Parser parser, XML_StartDoctypeDeclHandler start, XML_EndDoctypeDeclHandler end)
void XML_SetElementDeclHandler(XML_Parser parser, XML_ElementDeclHandler eldecl)
void XML_SetAttlistDeclHandler(XML_Parser parser, XML_AttlistDeclHandler attdecl)
void XML_SetEntityDeclHandler(XML_Parser parser, XML_EntityDeclHandler handler)
void XML_SetUnparsedEntityDeclHandler(XML_Parser parser, XML_UnparsedEntityDeclHandler handler)
void XML_SetNotationDeclHandler(XML_Parser parser, XML_NotationDeclHandler handler)
void XML_SetNotStandaloneHandler(XML_Parser parser, XML_NotStandaloneHandler handler)
enum XML_Error XML_GetErrorCode(XML_Parser parser)
const XML_LChar * XML_ErrorString(enum XML_Error code)
long XML_GetCurrentByteIndex(XML_Parser parser)
XML_Size XML_GetCurrentLineNumber(XML_Parser parser)
XML_Size XML_GetCurrentColumnNumber(XML_Parser parser)
int XML_GetCurrentByteCount(XML_Parser parser)
const char * XML_GetInputContext(XML_Parser parser, int * offset, int * size)
void XML_SetUserData(XML_Parser parser, void * p)
void XML_DefaultCurrent(XML_Parser parser)
void XML_UseParserAsHandlerArg(XML_Parser parser)
enum XML_Status XML_SetBase(XML_Parser parser, const XML_Char * p)
const XML_Char * XML_GetBase(XML_Parser parser)
int XML_GetSpecifiedAttributeCount(XML_Parser parser)
int XML_GetIdAttributeIndex(XML_Parser parser)
enum XML_Status XML_SetEncoding(XML_Parser parser, const XML_Char * encodingName)
int XML_SetParamEntityParsing(XML_Parser parser, enum XML_ParamEntityParsing parsing)
void XML_SetReturnNSTriplet(XML_Parser parser, int do_nst)
const XML_LChar * XML_ExpatVersion()
XML_Expat_Version XML_ExpatVersionInfo()
XML_Bool XML_ParserReset(XML_Parser parser, const XML_Char * encodingName)
void XML_SetSkippedEntityHandler(XML_Parser parser, XML_SkippedEntityHandler handler)
enum XML_Error XML_UseForeignDTD(XML_Parser parser, XML_Bool useDTD)
const XML_Feature * XML_GetFeatureList()
enum XML_Status XML_StopParser(XML_Parser parser, XML_Bool resumable)
enum XML_Status XML_ResumeParser(XML_Parser parser)
void XML_GetParsingStatus(XML_Parser parser, XML_ParsingStatus * status)
void XML_FreeContentModel(XML_Parser parser, XML_Content * model)
void * XML_MemMalloc(XML_Parser parser, size_t size)
void * XML_MemRealloc(XML_Parser parser, void * ptr, size_t size)
void XML_MemFree(XML_Parser parser, void * ptr)
##end functionlist
