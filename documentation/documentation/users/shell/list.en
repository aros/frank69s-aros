====
List
====

.. This document is automatically generated. Don't edit it!

`Index <index>`_ `Prev <liblist>`_ `Next <load>`_ 

---------------

Name
~~~~
::


     List


Format
~~~~~~
::


     List [(dir | pattern | filename)] [ PAT (pattern)] [KEYS] [DATES]
          [NODATES] [TO (name)] [SUB (string)] [SINCE (date)] [UPTO (date)]
          [QUICK] [BLOCK] [NOHEAD] [FILES] [DIRS] [LFORMAT (string)] [ALL]


Template
~~~~~~~~
::


     DIR/M,P=PAT/K,KEYS/S,DATES/S,NODATES/S,TO/K,SUB/K,SINCE/K,UPTO/K,QUICK/S,BLOCK/S,NOHEAD/S,FILES/S,DIRS/S,LFORMAT/K,ALL/S


Location
~~~~~~~~
::


     C:


Function
~~~~~~~~
::


     Lists detailed information about the files and directories in the 
     current directory or in the directory specified by DIR.

     The information for each file or directory is presented on a separate 
     line, containing the following information:

     name
     size (in bytes)
     protection bits
     date and time


Inputs
~~~~~~
::


     DIR           --  The directory to list. If left out, the current
                       directory will be listed.
     PAT           --  Display only files matching 'string'
     KEYS          --  Display the block number of each file or directory
     DATES         --  Always display the full modification date of files
                       and directories instead of a day name.
     NODATES       --  Don't display dates
     TO (name)     --  Write the listing to a file instead of stdout
     SUB (string)  --  Display only files, a substring of which matches
                       the substring 'string'
     SINCE (date)  --  Display only files newer than 'date'
     UPTO (date)   --  Display only files older than 'date'
     QUICK         --  Display only the names of files
     BLOCK         --  File sizes are in blocks of 512 bytes
     NOHEAD        --  Don't print any header information
     FILES         --  Display files only
     DIRS          --  Display directories only
     LFORMAT       --  Specify the list output in printf-style
     ALL           --  List the contents of directories recursively

     The following attributes of the LFORMAT strings are available

     %A  --  file attributes
     %B  --  size of file in blocks rather than bytes
     %C  --  file comment
     %D  --  modification date
     %E  --  file extension
     %F  --  volume name
     %K  --  file key block number
     %L  --  size of file in bytes
     %M  --  file name without extension
     %N  --  file name
     %P  --  file path
     %S  --  superceded by %N and %P; obsolete
     %T  --  modification time


Result
~~~~~~
::


     Standard DOS return codes.


Example
~~~~~~~
::


     1> List C:
     Directory "C:" on Wednesday 12-Dec-99
     AddBuffers                  444 --p-rwed 02-Sep-99 11:51:31
     Assign                     3220 --p-rwed 02-Sep-99 11:51:31
     Avail                       728 --p-rwed 02-Sep-99 11:51:31
     Copy                       3652 --p-rwed 02-Sep-99 11:51:31
     Delete                     1972 --p-rwed 02-Sep-99 11:51:31
     Execute                    4432 --p-rwed 02-Sep-99 11:51:31
     List                       5108 --p-rwed 02-Sep-99 11:51:31
     Installer                109956 ----rwed 02-Sep-99 11:51:31
     Which                      1068 --p-rwed 02-Sep-99 11:51:31
     9 files - 274 blocks used        


See also
~~~~~~~~

`Dir <dir>`_ 

